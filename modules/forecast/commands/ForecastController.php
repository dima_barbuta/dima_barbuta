<?php

namespace app\modules\forecast\commands;

use app\models\Cities;
use app\models\Forecast;

use DateTime;
use yii\console\Controller;
use linslin\yii2\curl;

class ForecastController extends Controller
{
    function validateDate($date, $format = 'd.m.Y')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    public function actionIndex()
    {
        echo "use 'yii forecast/forecast/get'";
    }

    public function actionGet($start, $end, $city)
    {

        if ($this->validateDate($start) && $this->validateDate($end)) {
            try {
                $duplicates = 0;
                $newRecord = 0;

                $curl = new curl\Curl();
                $response = $curl->get("http://quiz.dev.travelinsides.com/forecast/api/getForecast?start=$start&end=$end&city=$city");
                //parse xml to object
                $ob = simplexml_load_string($response);

                // find city in DB
                $cityDB = Cities::find()->where(['=', 'name', $city])->limit(1)->one();

                if ($cityDB) {
                    foreach ($ob->row as $row) {
                        // find duplicate in db
                        $duplicate = Forecast::find()
                            ->where(['=', 'city_id', $cityDB->id])
                            ->where(['=', 'when_created', $row->ts])
                            ->all();

                        if (!$duplicate) {
                            // insert data if duplicate not found
                            $forecast = new Forecast();
                            $forecast->city_id = $cityDB->id;
                            $forecast->temperature = (float)$row->temperature;
                            $forecast->when_created = "$row->ts";
                            $forecast->save();
                            $newRecord += 1;
                        } else {
                            $duplicates += 1;
                        }
                    }
                    echo 'New records inserted in DataBase: ' . $newRecord . PHP_EOL . 'Duplicates: ' . $duplicates;
                } else {
                    echo "Unknown City: '$city'";
                }

            } catch (\Exception $e) {
                echo $e;
            } catch (\Throwable $e) {
                echo $e;
            }
        } else {
            echo 'The date you entered is invalid';
        }

    }

}