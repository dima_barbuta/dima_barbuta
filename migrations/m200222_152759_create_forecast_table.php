<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%forecast}}`.
 */
class m200222_152759_create_forecast_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%forecast}}', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer(),
            'temperature' => $this->float(),
            'when_created' => $this->text()
        ]);
        $this->addForeignKey('country_id', 'forecast', 'city_id', 'cities', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%forecast}}');
    }
}
