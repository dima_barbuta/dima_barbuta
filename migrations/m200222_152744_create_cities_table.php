<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cities}}`.
 */
class m200222_152744_create_cities_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cities}}', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer(),
            'name' => $this->text()
        ]);


        $this->addForeignKey('country_id', 'cities', 'country_id', 'countries', 'id');


        $this->insert('cities', ['name' => 'Moscow', 'country_id' => 1]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cities}}');
    }
}
