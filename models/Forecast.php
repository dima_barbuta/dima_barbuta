<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "forecast".
 *
 * @property int $id
 * @property int|null $city_id
 * @property float|null $temperature
 * @property string|null $when_created
 *
 * @property Cities $city
 */
class Forecast extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'forecast';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id'], 'default', 'value' => null],
            [['city_id'], 'integer'],
            [['temperature'], 'number'],
            [['when_created'], 'string'],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'temperature' => 'Temperature',
            'when_created' => 'When Created',
        ];
    }

    /**
     * Gets query for [[City]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }
}
