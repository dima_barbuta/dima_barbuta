<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cities".
 *
 * @property int $id
 * @property int|null $country_id
 * @property string|null $name
 *
 * @property Countries $country
 * @property Forecast[] $forecasts
 */
class Cities extends \yii\db\ActiveRecord
{
    public $average;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['country_id'], 'default', 'value' => null],
            [['country_id'], 'integer'],
            [['name'], 'string'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => 'Country ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[Country]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'country_id']);
    }

    /**
     * Gets query for [[Forecasts]].
     *
     * @return \yii\db\ActiveQuery
     */

    public function getForecasts()
    {

    }

    public function getMaxtemp()
    {
        return $this->hasOne(Forecast::className(), ['city_id' => 'id'])->OrderBy(['temperature'=>SORT_DESC]);
    }

    public function getMintemp()
    {
        return $this->hasOne(Forecast::className(), ['city_id' => 'id'])->OrderBy(['temperature'=>SORT_ASC]);
    }
}
