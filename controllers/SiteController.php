<?php

namespace app\controllers;

use app\models\Cities;
use app\models\Forecast;
use app\models\StatsForm;
use DateTime;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionTest()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }


    public function actionStats()
    {

        $model = new StatsForm();
        return $this->render('stats', compact('model'));
    }


    public function actionHistory($city)
    {

        try {
            $cityDb = Cities::find()
                ->where(['like', 'name', ucfirst($city)])
                ->with('country')
                ->limit(1)
                ->one();

            $forecasts = Forecast::find()
                ->where(['city_id' => $cityDb->id])
                ->orderBy(['when_created' => SORT_DESC])
                ->asArray()
                ->all();


            /* Parse Query array into readable array */
            $parsedData = [];
            // TODO: set time zone
            date_default_timezone_set('utc');
            foreach ($forecasts as $key => $forecast) {
                $forecasts[$key]['date'] = (new DateTime())->setTimestamp($forecast['when_created'])->format('F d, Y');
                if ($key == 0) {
                    $parsedData[$forecasts[$key]['date']] = [];
                    array_push($parsedData[$forecasts[$key]['date']],
                        [
                            'temperature' => $this->fahrenheit_to_celsius((float)$forecast['temperature']),
                            'time' => (new DateTime())->setTimestamp($forecast['when_created'])->format('H:i:s')
                        ]);

                } else {
                    if ($forecasts[$key]['date'] != $forecasts[$key - 1]['date']) {
                        $parsedData[$forecasts[$key]['date']] = [];
                        array_push($parsedData[$forecasts[$key]['date']],
                            [
                                'temperature' => $this->fahrenheit_to_celsius((float)$forecast['temperature']),
                                'time' => (new DateTime())->setTimestamp($forecast['when_created'])->format('H:i:s')
                            ]);
                    } else {
                        array_push($parsedData[$forecasts[$key]['date']],
                            [
                                'temperature' => $this->fahrenheit_to_celsius((float)$forecast['temperature']),
                                'time' => (new DateTime())->setTimestamp($forecast['when_created'])->format('H:i:s')
                            ]);
                    }
                }
            }
        } catch (\Exception $e) {
            return $e;
        }
        return $this->render('history', compact('cityDb', 'parsedData'));
    }


    function fahrenheit_to_celsius($given_value)
    {
        $celsius = (5 / 9 * ($given_value - 32));
        return $celsius;
    }
}
