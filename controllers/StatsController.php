<?php

namespace app\controllers;

use app\models\Cities;
use app\models\Countries;
use app\models\StatsForm;
use Yii;
use app\models\Forecast;
use app\models\ForecastSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StatsController implements the CRUD actions for Forecast model.
 */
class StatsController extends Controller
{

    public $average;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Forecast models.
     * @return mixed
     */
    public function actionIndex()
    {

        try {

            $request = Yii::$app->request->post('StatsForm');
            $request['start'] ? $start = strtotime($request['start']) : $start = 0;
            $request['end'] ? $end = strtotime($request['end']) : $end = strtotime('now');

            $dataProvider = new ActiveDataProvider([
                'query' => Cities::find()
                    ->select(['cities.id', 'cities.name', 'cities.country_id', 'AVG(forecast.temperature) AS average'])
                    ->groupBy(['cities.id'])
                    ->where(['>', 'forecast.when_created', $start])
                    ->where(['<', 'forecast.when_created', $end])
                    ->leftJoin('forecast', 'cities.id = forecast.city_id')
                    ->with(['country', 'maxtemp', 'mintemp'])
            ]);

            $model = new StatsForm();
            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'model' => $model,
                'start' => $start,
                'end' => $end
            ]);
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Displays a single Forecast model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Forecast model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Forecast();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Forecast model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Forecast model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Forecast model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Forecast the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Forecast::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
