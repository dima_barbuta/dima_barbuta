<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

$this->title = 'Stats';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-default">

    <div class="panel-heading">
        Search
    </div>


    <?php $form = ActiveForm::begin(['options' => ['id' => 'stats-form']]) ?>
    <div class="panel-body row">

        <div class="col-xs-2">
            <div class='input-group date' id='datetimepicker-start'>
                <?php

                $model->start = date("d.m.Y");
                echo $form->field($model, 'start',
                    ['template' =>
                        '{label}<div class="input-group dtp">{input} 
                                <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                         </div>
                         <div class="">{error}</div>',

                    ]) ?>
            </div>
        </div>


        <div class="col-xs-2">
            <div class='input-group date' id='datetimepicker-end'>
                <?php
                $model->end = date("d.m.Y");
                echo $form->field($model, 'end',
                    ['template' =>
                        '{label}<div class="input-group dtp">{input} 
                                <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                         </div>
                         <div class="">{error}</div>'
                    ]) ?>
            </div>
        </div>

        <div class="col-xs-2">
            <div class="form-group">
                <?php echo Html::submitButton('<span class="glyphicon glyphicon-search"></span> Search',
                    ['class' => 'btn btn-success', 'style' => 'margin-top: 25px']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end() ?>

</div>


<div class="panel panel-default">
    <div class="panel-body">

        <?php echo GridView::widget([
            'dataProvider' => $model,
            'columns' => [
                'id',
                'name',
                'created_at:datetime',
                // ...
            ],
        ]) ?>
    </div>
</div>