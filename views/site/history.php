<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'History';

$this->params['breadcrumbs'][] = $this->title;
?>


<div class="panel panel-default">
    <div class="panel-heading">
        History of <?php echo $cityDb->name ?> (<?php echo $cityDb->country->name ?>)
    </div>


    <div class="panel-body">
        <div class="row">
            <?php $step = 0; ?>
            <?php foreach ($parsedData as $key => $temperatures) { ?>
                <?php
                if ($step % 4 == 0) {
                    echo '</div><div class="row">';
                }
                $step++;
                ?>
                <div class="col-xs-3">
                    <p><strong><?php echo $key ?></strong></p>

                    <?php foreach ($temperatures as $item) {
                        ?>
                        <p><?php echo $item['time'] ?> <?php echo $item['temperature'] ?> &#8451;</p>
                    <?php } ?>
                </div>
                <?php
            } ?>
        </div>
    </div>
