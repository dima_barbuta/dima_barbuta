<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ForecastSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Forecasts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="forecast-index">


    <div class="panel panel-default">

        <div class="panel-heading">
            Search
        </div>


        <?php $form = ActiveForm::begin(['options' => ['id' => 'stats-form'], 'method' => 'post']) ?>
        <div class="panel-body row">

            <div class="col-xs-2">
                <div class='input-group date' id='datetimepicker-start'>
                    <?php

                    $start > 0 ? $model->start = date("d.m.Y", $start) : $model->start = date("d.m.Y");
                    echo $form->field($model, 'start',
                        ['template' =>
                            '{label}<div class="input-group dtp">{input} 
                                <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                         </div>
                         <div class="">{error}</div>',

                        ]) ?>
                </div>
            </div>


            <div class="col-xs-2">
                <div class='input-group date' id='datetimepicker-end'>
                    <?php
                    $model->end = date("d.m.Y", $end);
                    echo $form->field($model, 'end',
                        ['template' =>
                            '{label}<div class="input-group dtp">{input} 
                                <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                         </div>
                         <div class="">{error}</div>'
                        ]) ?>
                </div>
            </div>

            <div class="col-xs-2">
                <div class="form-group">
                    <?php echo Html::submitButton('<span class="glyphicon glyphicon-search"></span> Search',
                        ['class' => 'btn btn-success', 'style' => 'margin-top: 25px']) ?>
                </div>
            </div>
        </div>

        <?php ActiveForm::end() ?>

    </div>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,

        'columns' => [
            [
                'label' => 'Country',
                'attribute' => 'country.name',
            ],
            [
                'label' => 'City',
                'attribute' => 'name',
            ],

            [
                'label' => 'Max temperature',
                'attribute' => 'maxtemp.temperature',
            ],
            [
                'label' => 'Min temperature',
                'attribute' => 'mintemp.temperature',
            ],
            [
                'label' => 'Avg temperature',
                'attribute' => 'average',
                'format' => ['decimal', 2]
            ],

            ['class' => 'yii\grid\ActionColumn', 'header' => "Actions"],
        ],
    ]); ?>


</div>
